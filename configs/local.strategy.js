const express = require('express')
const router = express.Router()
const passport = require('passport')
const localStrategy = require('passport-local').Strategy;
const usuarios = require('../model/Usuarios');
const bcrypt = require('bcrypt');

passport.use(new localStrategy({ 
    passReqToCallback : true, usernameField: 'email', passwordField: 'password' },
function(req, username, password, done) {
        usuarios.findByMail(username).then(function(user){
        if (user.length === 0)
            return done(null, false, {erro: "Usuário ou senha incorretos"});

        let crypt = bcrypt.compareSync(password, user[0].senha);
        if  (crypt){
            return done(null, user[0])
        } else {
            return done(null, false, {erro: "Usuário ou senha incorretos"});
        }

    });
}));

passport.serializeUser(function(user, done) {
    return done(null, user.id);
 });

 passport.deserializeUser(function(id, done) {
    usuarios.findById(id).then(function(user){
       return done(null, user[0]);
    });
});