const knex = require('knex')
const knexConfigs = require('../knexfile')
const db = knex(knexConfigs.development)

const TABLE_NAME = 'usuarios'

module.exports = {
    get() {
        return db(TABLE_NAME).select('*')
    },
    insert(usuario) {
        return db(TABLE_NAME).insert(usuario);
    },
    delete(usuario) {
        return db(TABLE_NAME)
            .where('id', usuario.id)
            .del();
    },
    update(usuario) {
        return db(TABLE_NAME)
            .where('id', usuario.id)
            .update({
                nome: usuario.nome,
            });
    },
    findById(id){
        return db(TABLE_NAME).select('*')
            .where('id', id)

    },   
    findByMail(email){
        return db(TABLE_NAME).select('*')
            .where('email', email);
    },
}