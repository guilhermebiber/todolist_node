const knex = require('knex')
const knexConfigs = require('../knexfile')
const db = knex(knexConfigs.development)

const TABLE_NAME = 'listas'


module.exports = {
    get() {
        return db(TABLE_NAME).select('*')
    },
    insert(lista) {
        return db(TABLE_NAME).insert(lista);
    },
    delete(id) {
        return db(TABLE_NAME)
            .where('id', id)
            .delete();
    },
    update(lista) {
        return db(TABLE_NAME)
            .where('id', lista.id)
            .update({
                nome: lista.nome,
            });
    },
    findById(id){
        return db(TABLE_NAME).select('*')
            .where('id', id)

    },
    findByIdLista(id){
        return db(TABLE_NAME).select('*')
            .where('id', id).first()

    },
    findByLista(id){
        return db(TABLE_NAME).select('*')
            .where('lista_id', id)

    },getListasUsuario(id_usuario) {
        return db(TABLE_NAME).column(
            ['listas.id', 'listas.nome']
        ).count('tarefas.id as cont').select().leftJoin('tarefas', 'listas.id'
            , 'tarefas.lista_id')
        .where({usuario_id:id_usuario}).groupBy('listas.id')
    },
}