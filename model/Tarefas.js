const knex = require('knex')
const knexConfigs = require('../knexfile')
const db = knex(knexConfigs.development)

const TABLE_NAME = 'tarefas'


module.exports = {
    get() {
        return db(TABLE_NAME).select('*')
    },
    insert(tarefa) {
        return db(TABLE_NAME).insert(tarefa);
    },
    delete(tarefa) {
        return db(TABLE_NAME)
            .where('id', tarefa.id)
            .del();
    },
    deleteByLista(lista_id) {
        return db(TABLE_NAME)
            .where('lista_id', lista_id)
            .del();
    },
    update(tarefa) {
        return db(TABLE_NAME)
            .where('id', tarefa.id)
            .update({
                nome: tarefa.nome,
                descricao: tarefa.descricao,
                finalizado: tarefa.finalizado
            });
    },
    findById(id){
        return db(TABLE_NAME).select('*')
            .where('id', id)

    },
    findByIdTarefa(id){
        return db(TABLE_NAME).select('*')
            .where('id', id).first()

    },
    findByListaId(id){
        return db(TABLE_NAME).select('*')
            .where('lista_id', id)

    },
    updateFinalizar(tarefa) {
        return db(TABLE_NAME)
            .where('id', tarefa.id)
            .update({
                finalizado: tarefa.finalizado
            });
    }
}