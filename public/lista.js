
$(document).on("click","#addLista",function(){
    $("#modalLista").modal("show");
})

$(document).on("click","#salvar",function(){
    nome = $("#nome").val()    

    lista = {
        'nome': nome
    }
    
    id = $("#id").val()

    if(id){
        update(id,lista)
    }else{
        criar(lista);
    }
    
})

$(document).on("click",'.editar',function(){

    id = $(this).data('id');  
    
    $("#modalLista").modal('show')

    carregarLista(id);

})

$(document).on("click",".apagar",function(){
    id = $(this).data("id");
    deletar(id);
})

$('#modalLista').on('hidden.bs.modal', function () {
    start()
})



function carregarLista(id){
    $.ajax({
        url:'/index/lista/'+id,
        type:'get',
        dataType:'json',
        success:function(data){
            nome = data.nome;
            id = data.id;

            $("#nome").val(nome);
            $("#id").val(id);
        }
    })
}

function update(id,lista){
    $.ajax({
        url:"/index/lista/"+id,
        type:"put",
        dataType:"json",
        data:lista,
        success:function(data){
            start();
        }
    })
}

function deletar(id){
    $.ajax({
        url:"/index/lista/"+id,
        type:"delete",
        dataType:"json",
        success:function(data){
            start();
        }
    })
}


function start(){
    
    $("input").val("");
    $("#modalLista").modal("hide")
    carregarListas();
}

function carregarListas(){

    $.ajax({
        url:'/index/lista',
        type:'get',
        dataType:'json',
        success:function(data){
            div = '';
            $.each(data,function(key,value){
                div+=`<div class="col-md-4">
                    <div class="card text-center">
                    <div class="card-body btnLista">
                    <h5 class="card-title lista"><a href='index/tarefas/`+value.id+`'>`+value.nome+`</a></h5>
                    <h6 class="card-subtitle mb-2 text-muted"> `+ value.cont +` tarefas</h6>
                    <p class="card-text"></p>
                    <a href="javascript:void(0)" data-id=`+value.id+` class="apagar card-link"><i class="fas fa-trash"></i></a>
                    <a href="javascript:void(0)" data-id=`+value.id+` class="editar card-link"><i class="fas fa-edit"></i></a>
                    </div>
                    </div>
                    </div>`;
            })
            $('.grid').html(div)

        }
    })
}

function criar(lista){

    $.ajax({
        url:'/index/lista',
        type:'POST',
        data:lista,
        dataType:'json',
        success:function(data){
            start();
        }
    });

}

$(document).ready(function(){
    start();
});