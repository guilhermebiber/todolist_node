
$(document).on("click","#addTarefa",function(){
    $("#modalTarefa").modal("show");
})

$(document).on("click","#salvar",function(){
    
    nome = $("#nome").val()
    lista_id = $("#lista_id").val()
    descricao = $("#descricao").val()
    

    tarefa = {
        'nome': nome,
        'lista_id':lista_id,
        'descricao':descricao
    }
    
    id = $("#id").val()

    if(id){
        update(id,tarefa)
    }else{
        criar(tarefa);
    }
    
})

$(document).on("click",'.editar',function(){

    id = $(this).data('id');  
    
    $("#modalTarefa").modal('show')

    carregarTarefa(id);

})

$(document).on("click",".apagar",function(){
    id = $(this).data("id");
    deletar(id);
})

$('#modalTarefa').on('hidden.bs.modal', function () {
    start()
})

$(document).on("click",".finalizado",function(){
    
    id = $(this).data('id');
    
    finalizar(id);
})

$(document).on("click",".pendente",function(){
    
    id = $(this).data('id');
    
    pendente(id);

})

function pendente(id){
    $.ajax({
        url:"/index/tarefa/pendente/"+id,
        type:"put",
        dataType:"json",
        success:function(data){
            start()
        }
    })
}

function finalizar(id){
    $.ajax({
        url:"/index/tarefa/finalizar/"+id,
        type:"put",
        dataType:"json",
        success:function(data){
            start()
        }
    })
}

function carregarTarefa(id){
    $.ajax({
        url:'/index/tarefa/'+id,
        type:'get',
        dataType:'json',
        success:function(data){

            nome = data.nome;
            lista_id = data.lista_id;
            descricao = data.descricao;

            id = data.id;

            $("#id").val(id);

            $("#nome").val(nome);
            $("#lista_id").val(lista_id);
            $("#descricao").val(descricao);

        }
    })
}

function update(id,tarefa){
    $.ajax({
        url:"/index/tarefa/"+id,
        type:"put",
        dataType:"json",
        data:tarefa,
        success:function(data){
            if(data.fail){
                
                $("#msgTela").show();
                $("#msgTela").html(data.msg)
                
            }else{
                start();
            }
            
        }
    })
}

function deletar(id){
    $.ajax({
        url:"/index/tarefa/"+id,
        type:"delete",
        dataType:"json",
        success:function(data){
            start();
        }
    })
}


function start(){
    
    $("#msgTela").hide();
    $("input").not('#lista_id').val("");
    $("textarea").val("");
    $("#modalTarefa").modal("hide")
    carregarTarefas();
}

function carregarTarefas(){

    lista_id = $("#lista_id").val()

    $.ajax({
        url:'/index/tarefas/lista/'+lista_id,
        type:'get',
        dataType:'json',
        success:function(data){
            div = '';
            $.each(data,function(key,value){
                status = "Pendente"
                
                statusLink = '<a href="javascript:void(0)" data-id='+value.id+' class="finalizado card-link"><i class="far fa-square"></i></a>';
                color="#FFF5EE";
                if(value.finalizado){
                    status = "Finalizado"
                    color = "#F0F8FF"
                    statusLink = '<a href="javascript:void(0)" data-id='+value.id+' class="pendente card-link"><i class="far fa-check-square"></i></a>';
                }

                div+=`<div class="col-md-6">
                    <div class="card text-center" style="background-color:`+color+`">
                    <div class="card-body btnLista">
                    <h4 class="card-title lista">`+value.nome+`</h4>
                    <h6 class="card-subtitle mb-2 text-muted">`+status+`</h6>
                    <p class="card-text">`+value.descricao+`</p>
                    <a href="javascript:void(0)" data-id=`+value.id+` class="apagar card-link"><i class="fas fa-trash"></i></a>
                    <a href="javascript:void(0)" data-id=`+value.id+` class="editar card-link"><i class="fas fa-edit"></i></a>
                    `+statusLink+`
                    </div>
                    </div>
                    </div>`;
            })
            $('.grid').html(div)

        }
    })
}

function criar(tarefa){

    $.ajax({
        url:'/index/tarefas',
        type:'POST',
        data:tarefa,
        dataType:'json',
        success:function(data){

            if(data.fail){

                $("#msgTela").show();
                $("#msgTela").html(data.msg)
            }else{
                start();
            }
            
        }
    });

}

$(document).ready(function(){
    start();
});