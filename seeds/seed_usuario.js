
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('usuarios').del()
    .then(function () {
      // Inserts seed entries
      return knex('usuarios').insert([
        {id: 1, nome: 'usaurio 1', email: '1@example.com'},
        {id: 2, nome: 'usaurio 2', email: '2@example.com'},
        {id: 3, nome: 'usaurio 3', email: '3@example.com'},
        {id: 4, nome: 'usaurio 4', email: '4@example.com'},
        {id: 5, nome: 'usaurio 5', email: '5@example.com'}
      ]);
    });
};
