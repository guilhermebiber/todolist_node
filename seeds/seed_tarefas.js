
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('tarefas').del()
    .then(function () {
      // Inserts seed entries
      return knex('tarefas').insert([
        {id: 1, lista_id: '1', nome: 'tarefa1', descricao: 'desc tarefa 1', finalizado: true},
        {id: 2, lista_id: '1', nome: 'tarefa2', descricao: 'desc tarefa 2', finalizado: false},
        {id: 3, lista_id: '1', nome: 'tarefa3', descricao: 'desc tarefa 3', finalizado: false},
        {id: 4, lista_id: '3', nome: 'tarefa4', descricao: 'desc tarefa 4', finalizado: true},
        {id: 5, lista_id: '4', nome: 'tarefa5', descricao: 'desc tarefa 5', finalizado: false},
        {id: 6, lista_id: '5', nome: 'tarefa6', descricao: 'desc tarefa 6', finalizado: false},
      ]);
    });
};
