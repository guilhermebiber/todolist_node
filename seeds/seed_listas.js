
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('listas').del()
    .then(function () {
      // Inserts seed entries
      return knex('listas').insert([
        {id: 1, nome: 'Lista 1',usuario_id:1},
        {id: 2, nome: 'Lista 2',usuario_id:1},
        {id: 3, nome: 'Lista 3',usuario_id:1},
        {id: 4, nome: 'Lista 4',usuario_id:1},
        {id: 5, nome: 'Lista 5',usuario_id:1},
        {id: 6, nome: 'Lista 6',usuario_id:2},
        {id: 7, nome: 'Lista 7',usuario_id:3},
        {id: 8, nome: 'Lista 8',usuario_id:4},
        {id: 9, nome: 'Lista 9',usuario_id:5},
        {id: 10, nome: 'Lista 10',usuario_id:5},
        {id: 11, nome: 'Lista 11',usuario_id:5},
      ]);
    });
};
