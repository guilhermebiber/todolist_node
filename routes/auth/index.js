const express = require('express')
const router = express.Router()
//const facebookRouter = require('./facebook.js');
const LocalRouter = require('./local.js');


//router.use('/facebook', facebookRouter);
router.use('/local', LocalRouter);

module.exports = router;