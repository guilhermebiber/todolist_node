var express = require('express');
var router = express.Router();
var passport = require('passport');
var path = require('path');

router.post('/',function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) { return res.redirect('/'); }
        req.logIn(user, function(err) {
            if (err) { return next(err); }
            return res.redirect('/index');
        });
    })(req, res, next);
});


module.exports = router;
