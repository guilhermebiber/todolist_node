var express = require('express');
var router = express.Router();
var passport = require('passport');
var path = require('path');
const usuarios = require('../model/Usuarios');
const bcrypt = require('bcrypt');

router.get('/', function(req, res, next){
   res.render('profile');
 });

 router.post('/', function(req,res,next) {

     bcrypt.hash( req.body.password, 12,)
         .then(function(crypt){
             var user = {
                 nome: req.body.username,
                 email: req.body.email,
                 senha: crypt,
             }

             usuarios.insert(user).then(function(){});
             res.redirect('/');
         })

});   
        
module.exports = router;