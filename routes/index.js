var express = require('express');
var router = express.Router();
const Listas = require('../model/Listas');
const Tarefas = require('../model/Tarefas');
const usuarios = require('../model/Usuarios');

var listas = {};

/* GET home page. */
router.get('/',  
  require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
  (req, res) => {

  res.render('index', { title: "Listas" });
});

router.get('/tarefas/:id', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  id = req.params.id
  res.render('tarefas', { title: "Tarefas", lista_id: id, icon: "fa-chevron-circle-left" });
});

router.get('/tarefas/lista/:id', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  id = req.params.id;
  Tarefas.findByListaId(id)
    .then(function (tarefas) {
      res.status(200).json(tarefas);
    })
});

router.post('/tarefas', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {

  nome = req.body.nome;
  lista_id = req.body.lista_id;
  descricao = req.body.descricao;


  if(!nome.length){
    
    res.status(201).json({ msg: "Preencher nome da Tarefa",fail: 1});
  }else{
    tarefa = {
      nome: nome,
      lista_id: lista_id,
      descricao: descricao
    }
  
    Tarefas.insert(tarefa)
      .then(function () {
        res.status(201).json({ msg: "ok" ,fail: 0});
      });
  }


})

router.get('/tarefa/:id', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  
  id = req.params.id;
  Tarefas.findByIdTarefa(id)
    .then(function (tarefas) {
      res.status(200).json(tarefas);
    });
});

router.delete('/tarefa/:id', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  id = req.params.id;
  lista.id = id;
  Tarefas.delete(lista)
    .then(function () {
      res.status(200).json({ msg: "ok" });
    });
})

router.put('/tarefa/:id', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  id = req.params.id;
  nome = req.body.nome;
  descricao = req.body.descricao;

  tarefa = {
    id: id,
    nome: nome,
    descricao: descricao
  }

  Tarefas.findById(id)
    .then(function(t){

      if(!t[0].finalizado){

        Tarefas.update(tarefa)
        .then(function () {
          res.status(200).json({ msg: "ok",fail:0 });
        });

      }else{
        res.status(200).json({ msg: "Atenção, tareja já finalizada", fail:1 });
      }
    })

})

router.put('/tarefa/finalizar/:id', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  id = req.params.id;

  tarefa = {
    id: id,
    finalizado: true
  }  
  Tarefas.updateFinalizar(tarefa)
    .then(function () {
      res.status(200).json({ msg: "ok" });
    });
})

router.put('/tarefa/pendente/:id', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  
  id = req.params.id;

  tarefa = {
    id: id,
    finalizado: false
  }  
  Tarefas.updateFinalizar(tarefa)
    .then(function () {
      res.status(200).json({ msg: "ok" });
    });

})

router.get('/lista', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {

  listas = Listas.getListasUsuario(req.user.id)
    .then(function (listas) {
      res.status(200).json(listas);
    })
})

router.get('/lista/:id', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  id = req.params.id;
  listas = Listas.findByIdLista(id)
    .then(function (lista) {
      res.status(200).json(lista);
    })
})

router.delete('/lista/:id', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  id = req.params.id;
  Listas.delete(id)
    .then(function () {

      Tarefas.deleteByLista(id)
        .then(function(){
          res.status(200).json({ msg: "ok" });
        })
      
    })
})

router.put('/lista/:id', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  id = req.params.id;
  lista = {};
  lista.id = id;
  lista.nome = req.body.nome;


  Listas.update(lista)
    .then(function () {
      res.status(200).json({ msg: "ok" });
    });
})

router.post('/lista', require('connect-ensure-login').ensureLoggedIn({redirectTo:'/'}), 
(req, res) => {
  
  // console.log(req.user.id);
  nome = req.body.nome;
  id = req.user.id;
  
  lista = {
    nome:nome,
    usuario_id:id,
  }

  Listas.insert(lista)
    .then(function () {
      res.status(201).json({ msg: "ok" });
    });
})

module.exports = router;
