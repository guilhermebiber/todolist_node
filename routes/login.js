var express = require('express');
var router = express.Router();
var passport = require('passport');

/* GET home page */
router.get('/', function(req, res, next) {
  res.render('login');
});

//router.post("/", passport.authenticate('local'));

/*router.post('/', 
 passport.authenticate('local', {
     successRedirect: '/index',
     failureRedirect:'/'
 }) );*/

module.exports = router;
