
exports.up = function(knex) {

    return knex.schema
        .createTable('usuarios', table => {
            table.increments('id').primary()
            table.string('nome')
            table.string('email')
            table.string('senha')
        })
        .createTable('listas', table => {
            table.increments('id').primary()
            table.string('nome')
            table.integer('usuario_id')
            table.foreign('usuario_id').references('id').inTable('usuarios')
        })
        .createTable('tarefas', table => {
            table.increments('id').primary()
            table.integer('lista_id')
            table.string('nome')
            table.string('descricao')
            table.boolean('finalizado').defaultTo(false)
            table.foreign('lista_id').references('id').inTable('listas')
        })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('usuarios')
        .dropTableIfExists('listas')
        .dropTableIfExists('tarefas')
};
